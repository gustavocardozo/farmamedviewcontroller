package main;

import bussiness.*;
import controller.NotificationViewTestController;
import comunication.PamiCodeCircuit;
import comunication.PamiCodeGateway;
import comunication.PamiCodeSupplier;
import comunication.PharmacyGateway;
import model.Pharmacy;
import model.Remedy;
import view.NotificationViewTest;

public class main {
	private static String URL_PAMI_OK = "http://www.mocky.io/v2/5cb54968330000da0c5d7a18";
	private static String METHOD = "GET";

	public static void main(String[] args) {
		//Test Notifier
		NotificationManager manager = new NotificationManager();
		NotificationViewTest view = new NotificationViewTest();
		NotificationViewTestController controller = new NotificationViewTestController(view, manager);
		manager.addObserver(controller);
		view.show();
		
		
		// Test connection
		System.out.println("------------");
		Remedy remedy = new Remedy("Ibuprofeno", "", "");
		System.out.println("Farmacias donde tiene disponible el medicamento: "+ remedy.getDrug());
		PharmacyGateway pharmacyGateway = new PharmacyGateway();
		java.util.List<Pharmacy> pharmacies = pharmacyGateway.getByRemedy(remedy);
		for (Pharmacy r : pharmacies) {
			System.out.println(r.getName());	
		}
	
		
		// Test connection
		System.out.println("------------");
		PamiCodeGateway pami = new PamiCodeGateway(new PamiCodeCircuit(new PamiCodeSupplier(URL_PAMI_OK, METHOD), PamiCodeCircuit.Status.CLOSED));
		boolean res = pami.validate("xxxxxxxx122121221");// always return true
		System.out.println("C�digo pami V�lido: " + res);	

	}

}
