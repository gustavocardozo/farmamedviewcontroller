package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JLabel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.apache.commons.math3.genetics.SelectionPolicy;

import bussiness.NotificationManager;
import bussiness.Notifier;
import model.Notification;
import model.User;
import view.NotificationViewTest;

public class NotificationViewTestController implements ActionListener, MouseListener, Observer{
	
	private NotificationViewTest notificationView;
	private NotificationManager notificationManager;
	private int countNotification = 0;
	private User user = new User("User1", "UserTest1", 1234, false, false);
	
	public NotificationViewTestController(NotificationViewTest view, NotificationManager manager) {
		notificationView  = view;
		notificationManager = manager;
		notificationView.setController(this);
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		int index = notificationView.getListNotification().getSelectedIndex();
		cleanNotifications(index);
	}

	@Override
	public void mouseEntered(MouseEvent e) {}

	@Override
	public void mouseExited(MouseEvent e) {}

	@Override
	public void mousePressed(MouseEvent e) {}

	@Override
	public void mouseReleased(MouseEvent e) {}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == notificationView.getBtnEvento()) {
			Notification n = new Notification("Notification N" + countNotification);
//			notificationManager.associateNotificationWithUser(user, n);
			notificationManager.notifyUser(user, n);
			countNotification++;
		}
		
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		Notification n = (Notification) arg1;
		notificationView.createNotification(n.getNotificationMessage());
		System.out.println(n.getNotificationMessage());
		notificationManager.notificationReceived(n.getStatus());
	}
	
	public void cleanNotifications(int index) {
		notificationView.removeNotification(index);
		notificationManager.notificationClean(user, user.getNotifications().get(index));
		System.out.println("Cantidad de notificaciones despues de eliminar: " + user.getNotifications().size());
	}

}
