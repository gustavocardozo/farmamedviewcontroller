package view;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.util.ArrayList;
import java.awt.Cursor;
import javax.swing.border.LineBorder;
import controller.NotificationViewTestController;
import java.awt.Color;
import javax.swing.SwingConstants;
import javax.swing.JList;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.AbstractListModel;
import javax.swing.DefaultListModel;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ListSelectionModel;

public class NotificationViewTest {

	private JFrame frame;
	private JButton btnEvento;
	private JPanel panelNotifications;
	private NotificationViewTestController controller;
	private JList<Object> listNotification;
	private DefaultListModel<Object> listModel;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NotificationViewTest window = new NotificationViewTest();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public NotificationViewTest() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 465, 379);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(223, 0, 226, 340);
		frame.getContentPane().add(scrollPane);
		
		panelNotifications = new JPanel();
		scrollPane.setViewportView(panelNotifications);
		panelNotifications.setLayout(new GridLayout(0, 1, 5, 5));
		
		JScrollPane scrollPane_1 = new JScrollPane();
		panelNotifications.add(scrollPane_1);
		
		listNotification = new JList<Object>();
		listNotification.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listModel = new DefaultListModel<Object>();
		listNotification.setModel(listModel);
		scrollPane_1.setViewportView(listNotification);
		
		btnEvento = new JButton("evento");
		btnEvento.setBounds(10, 137, 89, 23);
		frame.getContentPane().add(btnEvento);
	}
	
	public void createNotification(String notificationMessage) {
		listModel.addElement(notificationMessage);
		reloadNotifications();
	}
	
	public void removeNotification(int index) {
		listModel.remove(index);
		reloadNotifications();
	}
	
	public void reloadNotifications() {
		listNotification.updateUI();
	}

	public JButton getBtnEvento() {
		return btnEvento;
	}

	public void setController(NotificationViewTestController controller) {
		this.controller = controller;
		btnEvento.addActionListener(controller);
		listNotification.addMouseListener(controller);
	}
	
	public void show() {
		frame.show();
	}
	
	public JList<Object> getListNotification() {
		return listNotification;
	}
}
